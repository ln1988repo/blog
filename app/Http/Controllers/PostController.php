<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        return view('post');
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'text' => 'required',
        ]);
        $post = new Post([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'author' => $request->get('author'),
            'active' => '0'
        ]);
        $post->save();
        return redirect()->route('create')->with('success', 'Post added.');
    }

    public function show($id)
    {   
        $post = Post::find($id);
        return view('post.show')->with('post', $post)->with('id', $id);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class AdminController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('admin.welcome')->with('posts', $posts);
    }

    public function show($id)
    {   
        $post = Post::find($id);
        return view('admin.show')->with('post', $post)->with('id', $id);
    }

    
    public function activate(Request $request)
    {
        var_dump($request->id);
        $post = Post::find($request->id);
        $post->active = "1";
        $post->save();
        return redirect()->route('admin')->with('success', 'Post has been activated!');
    }
    
    public function edit($id)
    {
        $post = Post::find($id);
        return view('admin.edit')->with('post', $post);
    }


    public function update(Request $request)
    {
        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->text = $request->text;
        $post->active = $request->active;
        $post->save();
        return redirect()->route('admin.edit', [$request->id])->with('success', 'Post edited.');   
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('admin')->with('success', 'Post has been successfully removed.');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PostController;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use App\Models\Post;

Paginator::useBootstrap();

class HomeController extends Controller
{
    public function index()
    {
      $posts = Post::orderBy('created_at', 'desc')->where('active', '>', 0)->paginate(2);
      return view('home')->with("posts", $posts);
    }
}

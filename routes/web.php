<?php

use Illuminate\Support\Facades\Route;

Auth::routes();


Route::view('/', 'welcome');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/post/create', [App\Http\Controllers\PostController::class, 'create'])->name('create');
    Route::post('/post/store', [App\Http\Controllers\PostController::class, 'store'])->name('store');
    Route::get('/post/show/{id}', [App\Http\Controllers\PostController::class, 'show'])->name('show');

    Route::group(['middleware' => 'role:superadministrator'], function () {
        Route::match(['get', 'post'], '/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
        Route::post('/admin/activate', [App\Http\Controllers\AdminController::class, 'activate'])->name('activate');
        Route::get('/admin/edit/{id}', [App\Http\Controllers\AdminController::class, 'edit'])->name('admin.edit');
        Route::match(['get', 'delete'], '/admin/show/{id}', [App\Http\Controllers\AdminController::class, 'show'])->name('admin.show');
        Route::post('/admin/update', [App\Http\Controllers\AdminController::class, 'update'])->name('update');
        Route::delete('/admin/delete/{id}', [App\Http\Controllers\AdminController::class, 'destroy'])->name('delete');
    });
});




@extends('layouts.app')

@section('content')

<div class="w-50 mx-auto py-5">
    <div>
        <h2 class="pt-3">Tell us what's on your mind about Lorem Ipsum!</h2>
    </div>

    <div class="d-flex flex-column">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(\Session::has('success'))
            <div class="alert alert-success">
                <p class="mt-3">{{\Session::get('success')}}</p>
            </div>
        @endif
        <form action="{{route('store')}}" method="post" class="d-flex flex-column">
            <label for="title" class="p-1 m-1">Title</label>
            <input type="text" name="title" id="title" class="p-1 m-1">
            <label for="text" class="p-1">Tell us what you got to say</label>
            <textarea name="text" id="text" cols="30" rows="10" class="p-1 m-1"></textarea>
            <input type="hidden" name="author" value="{{ Auth::user()->name }}">
            <button type="submit" name="post" value="post" class="w-25 p-2 m-2 btn-success">Post</button>
            @csrf
        </form>
    </div>
</div>
@endsection


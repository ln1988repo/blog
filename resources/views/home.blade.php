@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row w-100">
        <div class="w-100">
            <div class="py-4">
                <a href="{{ url('/post/create') }}" class="text-md text-gray-700 h2">New Post</a>
            </div>
            <div class="card">
                <div class="card-header">{{ __('Here you can see all Posts about Lorem Ipsum') }}</div>
                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p class="mt-3">{{\Session::get('success')}}</p>
                    </div>
                @endif
                <div class="">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                </div>

                @if(isset($posts))
                @foreach($posts as $post)
                    @if($post->active === 1)
                    <div class="bg-dark py-5">
                        <div class="w-75 mx-auto bg-light py-5">
                            <div class="d-flex flex-column">
                                <h3 class="text-center h1 text-break">
                                    <a href="post/show/{{$post->id}}">{{$post->title}}</a>
                                </h3>
                                <h3 class="text-center my-3">{{$post->author}}</h3>
                                <h3 class="text-start ml-5 mr-3">{{ Str::limit($post->text, 150) }}</h3>
                                <h3 class="text-center h5 mt-5">{{$post->created_at}}</h3>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                <div class="mx-auto py-3" style="width:10%">{{$posts->links()}}</div>
                @endif
            </div>

        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')

<div class="w-75 mx-auto">
    <div class="w-75 mx-auto p-3">
        <a href="{{ url('/home') }}" class="text-md text-gray-700 p-4 h2">Home</a>
        <a href="{{ url('/post/create') }}" class="text-md text-gray-700 p-4 h2">Create Post</a>
    </div>
    <div class="p-5">
        <h1>Wellcome Admin!</h1>
    </div>
    @if(\Session::has('success'))
            <div class="alert alert-success">
                <p class="mt-3">{{\Session::get('success')}}</p>
            </div>
    @endif
    <div class="bg-dark">
            @if(count($posts) > 0)
                @foreach($posts as $post)
                <div class="bg-dark py-5">
                    <div class="w-75 mx-auto bg-light pt-5">
                        <div class="d-flex flex-column">
                            <h3 class="text-center h1 text-break">
                                <a href="admin/show/{{$post->id}}">{{$post->title}}</a>
                            </h3>
                            <h3 class="text-center my-3">{{$post->author}}</h3>
                            <h3 class="text-start ml-5 mr-3 text-break">{{$post->text}}</h3>
                            <h3 class="text-center h5 mt-5">{{$post->created_at}}</h3>
                            <h3 class="text-center h5 mt-2">
                                @if($post->active === 0)
                                    <span style='font-size:100px; color:red'>&#9679;</span>
                                    <form action="{{route('activate')}}" method="post">
                                        <input type="hidden" name="id" value="{{$post->id}}">
                                        <button type="submit" name="activate" class="w-25 bg-success mx-auto mb-3">Activate</button>
                                        @csrf
                                    </form>
                                @else
                                    <span style='font-size:100px; color:green'>&#9679;</span>
                                @endif
                            </h3>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>
</div>


@endsection
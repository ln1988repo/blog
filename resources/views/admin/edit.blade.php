@extends('layouts.app')

@section('content')

<div class="w-75 mx-auto">
    <div class="py-2">
        <div class="w-50 mx-auto pt-1"><a href="{{route('home')}}" class="h2">Home</a></div>
        <h2 class="text-center p-3">Edit your thoughts about Lorem Ipsum.</h2>
    </div>

    <div class="mx-auto w-50">
        @if(\Session::has('success'))
            <div class="alert alert-success">
                <p class="mt-3">{{\Session::get('success')}}</p>
            </div>
        @endif
        <form action="{{route('update')}}" method="POST" class="d-flex flex-column mx-auto">
            @csrf
            <input type="hidden" name="id" value="{{$post->id}}">
            <label for="title" class="p-1 m-1">Title</label>
            <input type="text" name="title" id="title" placeholder="Title" value="{{$post->title}}" class="p-1 m-1">
            <label for="text" class="p-1">Tell us what you got to say</label>
            <textarea name="text" id="text" cols="30" rows="10" placeholder="Thoughts" class="p-1 m-1">{{$post->text}}</textarea>
            <input type="hidden" name="{{ Auth::user()->name }}" class="p-1 m-1">
            <input type="hidden" name="active" value="0">
            <button type="submit" name="post" class="w-25 p-2 m-2 btn-warning">Update</button>
        </form>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')

<div class="bg-dark pb-5">
    <div class="w-25 mx-auto  pt-5"><a href="{{route('admin')}}" class="h1">Back to Home</a></div>
    <div class="w-25 mx-auto bg-light py-5 mt-5">
        <div class="d-flex flex-column w-100">
            <h3 class="text-center h1 text-break">{{$post->title}}</h3>
            <h3 class="text-center">{{$post->author}}</h3>
            <h3 class="text-start ml-3 text-break">{{$post->text}}</h3>
            <h3 class="text-center h5 mt-5">{{$post->created_at}}</h3>
        </div>
        <div class="w-25 ml-auto">
            <a href="{{route('admin.edit', $post->id)}}" class="btn btn-warning">Edit</a>
            <a href="{{route('delete', $post->id)}}" class="btn btn-danger">Delete</a>
        </div>
    </div>
</div>
@endsection
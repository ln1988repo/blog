@extends('layouts.app')

@section('content')

<div class="container" style="height: 82vh;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="p-5">
                <a href="{{ route('create') }}" class="h2">Create Post</a>
            </div>
        </div>
    </div>
</div>

@endsection
